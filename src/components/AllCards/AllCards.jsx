import { Component } from "react";
import PropTypes from 'prop-types';

import Card from "./components/Card";
import './AllCards.scss'
class AllCards extends Component{
    render(){
        const {handlerCurrentProd, handlerModal, data, handlerFavorite} = this.props;
        const mappedData = data.map((item, index) => <Card handlerFavorite={handlerFavorite} handlerCurrentProd={handlerCurrentProd} handlerModal={handlerModal} key={index} data={item} />)
        return(
            <>
                <div className="wrap">
                    <div className="cards" >
                        {data && mappedData}
                    </div>
                </div>
            </>
           
        )
    }
}
AllCards.propTypes = {
	handlerCurrentProd: PropTypes.func,
    handlerModal: PropTypes.func,
    handlerFavorite: PropTypes.func,
    data: PropTypes.array,
};
export default AllCards;