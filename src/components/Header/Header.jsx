import {Component} from 'react';
import PropTypes from 'prop-types';

import {ReactComponent as Logo } from "./icons/logo.svg";
import {ReactComponent as Cart } from "./icons/cart.svg";
import {ReactComponent as Favourites } from "./icons/favorites.svg";
import './Header.scss';

class Header extends Component{
    render() {
         	const {count, countFav} = this.props;
    
	return (
		<header className="header">
			<div className="container">
				<div className="header__wrapper">
					<div className="header__logo">
						<a href="#" className="logo">
							<Logo /><span className="logo--text">Sunny</span>
						</a>
					</div>
					<div className="header__actions">
						<div className="header__favorites-list">
							<span className="icon-favorite">
								<span className="count">{countFav}</span>
								<Favourites/>
							</span>
							Favorite
						</div>
						<div className="header__favorites-list">
							<span className="icon-favorite">
								<span className="count count--cart">{count}</span>
								<Cart/>
							</span>
							Shopping
						</div>
					</div>
				</div>
			</div>
		</header>
	)
}
}
Header.propTypes = {
	count: PropTypes.number,
	countFav: PropTypes.number,
};

export default Header;
