import {Component} from "react";
import PropTypes from 'prop-types';
import {ReactComponent as Cart} from './components/icons/cart.svg';
import './Button.scss';

class Button extends Component{
    render(){
        const {handlerClick} = this.props; 
        return(
       
            <div className="wrapper">
                <button className ="button" type="button" onClick={handlerClick}>
                    <span className="button--text">
                        Buy
                    </span>
                    <Cart/>
                    
                </button>
            </div>
           
       )
    }
}
Button.propTypes = {
	handlerClick: PropTypes.func,
};
export default Button;
